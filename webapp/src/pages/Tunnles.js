
import React from "react";
import {Button } from '@themesberg/react-bootstrap';
import {TunnelsTable } from "../components/Tables";


export default () => {
  return (
    <>
      <div className="d-xl-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
        <div className="d-block mb-4 mb-xl-0">
          <h4>Tunnels </h4>
          <p className="mb-0">
            Argo tunnels running through cloudflared.
          </p>
        </div>
        <div className="text-end">
            <Button variant="tertiary" size="sm">New Tunnel</Button>
        </div>
      </div>

      <TunnelsTable />
    </>
  );
};
