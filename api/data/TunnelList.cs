using System;

namespace api.data
{
    public class Tunnel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public DateTime Created { get; set; }

        public string Connections { get; set; }
    }
}
