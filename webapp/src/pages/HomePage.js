import React, { useState, useEffect } from 'react';
import { Route, Switch, Redirect } from "react-router-dom";
import { Routes } from "../routes";

// pages
import Tunnels from "./Tunnles";
import NotFoundPage from "./errors/NotFound";
import ServerError from "./errors/ServerError.js";

// components
import Sidebar from "../components/Sidebar";
import Preloader from "../components/Preloader";

const RouteWithSidebar = ({ component: Component, ...rest }) => {
  const [loaded, setLoaded] = useState(false);

  useEffect(() => {
    const timer = setTimeout(() => setLoaded(true), 1000);
    return () => clearTimeout(timer);
  }, []);

  return (
    <Route {...rest} render={props => (
      <>
        <Preloader show={loaded ? false : true} />
        <Sidebar />

        <main className="content">
          <Component {...props} />
        </main>
      </>
    )}
    />
  );
};

export default () => (
  <Switch>
    {/* pages */}
    <RouteWithSidebar exact path={Routes.Tunnels.path} component={Tunnels} />
    <RouteWithSidebar exact path={Routes.NotFound.path} component={NotFoundPage} />
    <RouteWithSidebar exact path={Routes.ServerError.path} component={ServerError} />

    <Redirect to={Routes.NotFound.path} />
  </Switch>
);
