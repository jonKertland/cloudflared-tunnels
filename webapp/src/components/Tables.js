
import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowDown, faArrowUp } from '@fortawesome/free-solid-svg-icons';
import { Card, Table} from '@themesberg/react-bootstrap';

import { tunnels} from "../data/tables";


export const TunnelsTable = () => {
  const TableRow = (props) => {
    const { tunnelName, subdomain, authorizedinCloudflare, status } = props;
    const upIcon = status ? faArrowDown : faArrowUp;
    const upTxtColor = status? "text-danger" : "text-success";
    let upText = status ? "Down" : "Up";

    return (
      <tr>
        <th scope="row">{tunnelName}</th>
        <td>{subdomain}</td>
        <td>{authorizedinCloudflare}</td>
        <td>
          <FontAwesomeIcon icon={upIcon} className={`${upTxtColor} me-3`} />
          {upText}
        </td>
      </tr>
    );
  };

  return (
    <Card border="medium" className="shadow-sm">
      <Table responsive className="align-items-center table-flush">
        <thead className="thead-tertiary">
          <tr>
            <th scope="col">Tunnel name</th>
            <th scope="col">Subdomain</th>
            <th scope="col">Authorization</th>
            <th scope="col">Status</th>
          </tr>
        </thead>
        <tbody>
          {tunnels.map(pv => <TableRow key={`tunnel-${pv.id}`} {...pv} />)}
        </tbody>
      </Table>
    </Card>
  );
};
