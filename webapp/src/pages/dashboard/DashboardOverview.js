
// import React from "react";
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faCashRegister, faChartLine, faCloudUploadAlt, faPlus, faTasks, faUserShield } from '@fortawesome/free-solid-svg-icons';
// import { Col, Row, Button, Dropdown, ButtonGroup } from '@themesberg/react-bootstrap';

// import { CounterWidget, CircleChartWidget, BarChartWidget, TeamMembersWidget, ProgressTrackWidget, RankingWidget, SalesValueWidget, SalesValueWidgetPhone, AcquisitionWidget } from "../../components/Widgets";
// import { PageVisitsTable } from "../../components/Tables";
// import { trafficShares, totalOrders } from "../../data/charts";

// export default () => {
//   return (
//     <>
//       <div className="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center py-4">
//         <Dropdown className="btn-toolbar">
//           <Dropdown.Toggle as={Button} variant="primary" size="sm" className="me-2">
//             <FontAwesomeIcon icon={faPlus} className="me-2" />New Task
//           </Dropdown.Toggle>
//           <Dropdown.Menu className="dashboard-dropdown dropdown-menu-left mt-2">
//             <Dropdown.Item className="fw-bold">
//               <FontAwesomeIcon icon={faTasks} className="me-2" /> New Task
//             </Dropdown.Item>
//             <Dropdown.Item className="fw-bold">
//               <FontAwesomeIcon icon={faCloudUploadAlt} className="me-2" /> Upload Files
//             </Dropdown.Item>
//             <Dropdown.Item className="fw-bold">
//               <FontAwesomeIcon icon={faUserShield} className="me-2" /> Preview Security
//             </Dropdown.Item>
//           </Dropdown.Menu>
//         </Dropdown>
//       </div>

//       <Row>
//         <Col xs={12} xl={12} className="mb-4">
//           <Row>
//             <Col xs={12} xl={8} className="mb-4">
//               <Row>
//                 <Col xs={12} className="mb-4">
//                   <PageVisitsTable />
//                 </Col>

//                 <Col xs={12} lg={6} className="mb-4">
//                   <TeamMembersWidget />
//                 </Col>

//                 <Col xs={12} lg={6} className="mb-4">
//                   <ProgressTrackWidget />
//                 </Col>
//               </Row>
//             </Col>

//             <Col xs={12} xl={4}>
//               <Row>
//                 <Col xs={12} className="mb-4">
//                   <BarChartWidget
//                     title="Total orders"
//                     value={452}
//                     percentage={18.2}
//                     data={totalOrders} />
//                 </Col>

//                 <Col xs={12} className="px-0 mb-4">
//                   <RankingWidget />
//                 </Col>

//                 <Col xs={12} className="px-0">
//                   <AcquisitionWidget />
//                 </Col>
//               </Row>
//             </Col>
//           </Row>
//         </Col>
//       </Row>
//     </>
//   );
// };
