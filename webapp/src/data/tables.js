
const tunnels = [
    { id: 1, stauts: "up", authorizedinCloudflare: "yes", subdomain: "hass.kertland.us", tunnelName: "Tunnel1" },
    { id: 2, status: "down", authorizedinCloudflare: "no", subdomain: "somethingelse.kertland.us", tunnelName: "Tunnel2" },
];

export {
    tunnels
};