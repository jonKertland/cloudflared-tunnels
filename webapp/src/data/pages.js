

import NotFoundImg from "../assets/img/pages/404.jpg";
import ServerErrorImg from "../assets/img/pages/500.jpg";

import { Routes } from "../routes";

export default [
    {
        "id": 1,
        "name": "Tunnels",
        "link": Routes.Tunnels.path
    },
    {
        "id": 2,
        "name": "404",
        "image": NotFoundImg,
        "link": Routes.NotFound.path
    },
    {
        "id": 3,
        "name": "500",
        "image": ServerErrorImg,
        "link": Routes.ServerError.path
    }
];