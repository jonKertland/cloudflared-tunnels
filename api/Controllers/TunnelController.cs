﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using api.data; 

namespace api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [ApiVersion("1.0")]
    public class TunnelController : ControllerBase
    {
        public TunnelController()
        {

        }

        [HttpPost("create")]
        public IActionResult CreateTunnel()
        {
            return Ok( new { success = true });
        }

        [HttpGet("list")]
        public IActionResult ListTunnels()
        {
            return Ok( new { success = true });
        }
    }
}
